package com.morqeo.shopmanagerassistant.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.morqeo.shopmanagerassistant.R;

import java.util.Objects;

public class SignInActivity extends AppCompatActivity {

    private TextInputLayout email;
    private TextInputLayout password;
    private FirebaseAuth firebaseAuth;
    private boolean loginMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        loginMode = true;
        firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth.getCurrentUser() != null) {
            startActivity(new Intent(SignInActivity.this, TodayTasksActivity.class));
        }

        email = findViewById(R.id.mail);
        password = findViewById(R.id.password);

    }

    public void userSignIn(View view) {

        String mailInput;
        String passwordInput;
        mailInput = Objects.requireNonNull(email.getEditText()).getText().toString().trim();
        passwordInput = Objects.requireNonNull(password.getEditText()).getText().toString().trim();

        firebaseAuth.signInWithEmailAndPassword(mailInput, passwordInput)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        startActivity(new Intent(
                                SignInActivity.this, TodayTasksActivity.class
                        ));
                    } else {
                        Toast.makeText(SignInActivity.this, "Wrong Email or passowrd. Access denied.",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public void userSignUp(View view){

        String mailInput;
        String passwordInput;
        mailInput = Objects.requireNonNull(email.getEditText()).getText().toString().trim();
        passwordInput = Objects.requireNonNull(password.getEditText()).getText().toString().trim();

        firebaseAuth.createUserWithEmailAndPassword(mailInput, passwordInput)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        startActivity(new Intent(
                                SignInActivity.this, FirstActionActivity.class
                        ));
                    } else {
                        Toast.makeText(SignInActivity.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void changeToSignUp(View view){
        TextView loggingText = findViewById(R.id.loggingText);
        TextView signUpLoginText = findViewById(R.id.signUpEmailText);
        Button loginButton = findViewById(R.id.loginButton);
        if(loginMode){
            loggingText.setText("SIGN UP");
            signUpLoginText.setText("I HAVE ACCOUNT, I WANT TO LOG IN");
            loginButton.setOnClickListener(this::userSignUp);
            loginButton.setText("SIGN UP");
        }else{
            loggingText.setText("LOG IN");
            signUpLoginText.setText("I DON'T HAVE ACCOUNT, I WANT TO SIGN UP");
            loginButton.setOnClickListener(this::userSignIn);
            loginButton.setText("SIGN IN");
        }
    }
}
