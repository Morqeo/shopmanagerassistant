package com.morqeo.shopmanagerassistant.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.morqeo.shopmanagerassistant.R;
import com.morqeo.shopmanagerassistant.models.User;
import com.morqeo.shopmanagerassistant.util.Reference;

public class FirstActionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_action);

    }

    public void saveName(View view){
        EditText editText = findViewById(R.id.name_of_the_user);
        String userName = editText.getText().toString();
        saveNewUser(userName);

        LinearLayout nameLinearLayout = findViewById(R.id.save_name_layout);
        nameLinearLayout.setVisibility(View.GONE);

        LinearLayout assignShopLayout = findViewById(R.id.assign_shop_layout);
        LinearLayout createShopLayout = findViewById(R.id.create_shop_layout);

        assignShopLayout.setVisibility(View.VISIBLE);
        createShopLayout.setVisibility(View.VISIBLE);
    }

    public void saveNewUser(String userName){
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        User user = new User();
        user.setName(userName);

        DatabaseReference firebaseDb = FirebaseDatabase.getInstance(Reference.REALTIME_DATABASE_URL).getReference();
        firebaseDb.child("users").child(firebaseUser.getUid()).setValue(user);
        Toast.makeText(FirstActionActivity.this,"Task Done!", Toast.LENGTH_SHORT).show();
    }

}
