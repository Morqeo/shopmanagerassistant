package com.morqeo.shopmanagerassistant.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.morqeo.shopmanagerassistant.DateManager;
import com.morqeo.shopmanagerassistant.R;

public class TodayTasksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today_tasks);

        setTitle(DateManager.getInstance().getFormattedDate());
    }

}
