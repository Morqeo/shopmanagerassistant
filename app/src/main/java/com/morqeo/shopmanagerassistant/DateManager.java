package com.morqeo.shopmanagerassistant;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateManager {

    private static DateManager instance;
    private static LocalDate localDate;


    private DateManager(){
        this.localDate = LocalDate.now();
    }

    public static DateManager getInstance(){
        if(instance == null){
            instance = new DateManager();
        }
        return instance;
    }

    public LocalDate getDate(){
        return localDate;
    }

    public void setDate(LocalDate localDate){
        this.localDate = localDate;
    }

    public String getFormattedDate(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        String formattedDate = localDate.format(formatter);
        return formattedDate;
    }

}
