package com.morqeo.shopmanagerassistant;

public enum Role {
    REGULAR_WORKER,
    SENIOR_REGULAR_WORKER,
    DEPUTY_MANAGER,
    MANAGER,
}
