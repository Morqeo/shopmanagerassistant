package com.morqeo.shopmanagerassistant.models;

import com.morqeo.shopmanagerassistant.Role;

public class User {

    private String name;
    private String shop;
    private Role role;


    public User(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
